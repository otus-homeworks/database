﻿using OtusTeaching.DB_Avito.Models;
using OtusTeaching.DB_Avito.Repository;
using System;
using System.Linq;

namespace OtusTeaching.DB_Avito.Seed
{
    static class SetTrialDataToDB
    {
        public static void Set(DBAvitoDataContext context)
        {
            // Инициализация начальными данными таблицы Person производится в DBAvitoDataContext.OnModelCreating
            // Данная инициализация выполняется только в двух случаях:
            // - При выполнении миграции. (При создании миграции добавляемые данные автоматически включаются в скрипт миграции)
            // - При вызове метода Database.EnsureCreated(), который создает БД при ее отсутствии


            //SetTrialDataToDB.SetTrialDataForPersons(context);
            SetTrialDataForAdvertisements(context);
            SetTrialDataForComments(context);
        }

        private static void SetTrialDataForPersons(DBAvitoDataContext db)
        {
            // Добавление записей в таблицу Person.            
            if (db.Persons.Any())
            {
                Console.Write("В таблице Persons есть данные. Если точно перезаписать нажмите Y : ");
                var key = Console.ReadKey();
                Console.WriteLine();
                if (key.KeyChar != 'Y' && key.KeyChar != 'y')
                    return;
                else
                    db.Persons.RemoveRange(db.Persons);
            }

            db.Persons.Add(new Person() { FirstName = "Первый", LastName = "Василий", Email = "firstEmail@gmail.com", Login = "1", Password = "1" });
            db.Persons.Add(new Person() { FirstName = "Второй", LastName = "Антон", Email = "secondEmail@yandex.ru", Login = "2", Password = "2" });
            db.Persons.Add(new Person() { FirstName = "Иванов", LastName = "Иван", Email = "thirdEmail@yandex.ru", Login = "3", Password = "3" });
            db.Persons.Add(new Person() { FirstName = "Петров", LastName = "Пётр", Email = "fourthEmail@yandex.ru", Login = "4", Password = "4" });
            db.Persons.Add(new Person() { FirstName = "Баширов", LastName = "Сидор", Email = "fifthEmail@yandex.ru", Login = "5", Password = "5" });

            db.SaveChanges();
            Console.WriteLine("Таблица Person заполнена пробными данными.");
        }

        private static void SetTrialDataForAdvertisements(DBAvitoDataContext db)
        {
            // Добавление записей в таблицу Advertisement.            
            if (db.Advertisements.Any())
                return;

            if (db.Persons.Count() < 4)
            {
                Console.WriteLine("Для заполнения таблицы Advertisement, необходимо наличие по крайней мере четырёх записей в таблице Person");
                return;
            }

            Person person1 = db.Persons.First();
            Person person2 = db.Persons.Skip(2).First();
            Person person3 = db.Persons.Skip(3).First();

            db.Advertisements.RemoveRange(db.Advertisements);

            db.Advertisements.Add(new Advertisement() { Category = AdCategory.Auto, Text = "Продам калину 2015г. пробег 100 тыс. км.", Cost = 100000, Person = person1, PersonId = person1.Id });

            db.Advertisements.Add(new Advertisement() { Category = AdCategory.Estate, Text = "Продаю гараж в центре", Cost = 90000, Person = person1, PersonId = person1.Id });
            db.Advertisements.Add(new Advertisement() { Category = AdCategory.Services, Text = "Поклею обои качественно и недорого", Cost = 1000, Person = person2, PersonId = person2.Id });
            db.Advertisements.Add(new Advertisement() { Category = AdCategory.Estate, Text = "Продаю 2х комн. кв. в центре", Cost = 1000000, Person = person2, PersonId = person2.Id });
            db.Advertisements.Add(new Advertisement() { Category = AdCategory.Job, Text = "Ищу сильного программиста. Чтобы мог и перенести что-нибудь.", Cost = 100000, Person = person3, PersonId = person3.Id });

            db.SaveChanges();
            Console.WriteLine("Таблица Advertisement заполнена пробными данными.");
        }

        private static void SetTrialDataForComments(DBAvitoDataContext db)
        {
            if (db.Сomments.Any())
                return;

            // Добавление записей в таблицу Advertisement.
            if (db.Persons.Count() < 4)
            {
                Console.WriteLine("Для заполнения таблицы Comments, необходимо наличие по крайней мере четырёх записей в таблице Person");
                return;
            }

            Person person1 = db.Persons.First();
            Person person2 = db.Persons.Skip(2).First();
            Person person3 = db.Persons.Skip(3).First();

            var comments = new Сomment[]
            {
                new() { Person = person1, Rating = RatingInComment.Fail, Text = "Покупал машину - после такси была, не советую.", PersonId = person1.Id },
                new() { Person = person1, Rating = RatingInComment.Excellent, Text = "Отличный продавец", PersonId = person1.Id },
                new() { Person = person2, Rating = RatingInComment.Satisfactory, Text = "Не очень хорошо обои поклеил.", PersonId = person2.Id },
                new() { Person = person2, Rating = RatingInComment.Bad, Text = "Недорого согласен, насчёт качественно нет", PersonId = person2.Id },
                new() { Person = person3, Rating = RatingInComment.Good, Text = "Всё в соответствии с объявлением.", PersonId = person3.Id }
            };

            db.Сomments.AddRange(comments);
            db.SaveChanges();
            Console.WriteLine("Таблица Сomment заполнена пробными данными.");
        }
    }
}
