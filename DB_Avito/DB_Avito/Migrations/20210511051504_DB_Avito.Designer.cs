﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using OtusTeaching.DB_Avito.Repository;

namespace DB_Avito.Migrations
{
    [DbContext(typeof(DBAvitoDataContext))]
    [Migration("20210511051504_DB_Avito")]
    partial class DB_Avito
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.5")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            modelBuilder.Entity("OtusTeaching.DB_Avito.Models.Advertisement", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<long>("Cost")
                        .HasColumnType("bigint");

                    b.Property<int>("PersonId")
                        .HasColumnType("integer");

                    b.Property<string>("Text")
                        .IsRequired()
                        .HasMaxLength(250)
                        .HasColumnType("character varying(250)");

                    b.HasKey("Id");

                    b.HasIndex("PersonId");

                    b.ToTable("Advertisements");
                });

            modelBuilder.Entity("OtusTeaching.DB_Avito.Models.Person", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Email")
                        .HasMaxLength(25)
                        .HasColumnType("character varying(25)");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(25)
                        .HasColumnType("character varying(25)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(25)
                        .HasColumnType("character varying(25)");

                    b.Property<string>("Login")
                        .HasMaxLength(12)
                        .HasColumnType("character varying(12)");

                    b.Property<string>("Password")
                        .HasMaxLength(12)
                        .HasColumnType("character varying(12)");

                    b.Property<string>("Phone")
                        .HasMaxLength(12)
                        .HasColumnType("character varying(12)");

                    b.HasKey("Id");

                    b.ToTable("Persons");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Email = "firstEmail@gmail.com",
                            FirstName = "Первый",
                            LastName = "Василий",
                            Login = "1",
                            Password = "1"
                        },
                        new
                        {
                            Id = 2,
                            Email = "secondEmail@yandex.ru",
                            FirstName = "Второй",
                            LastName = "Антон",
                            Login = "2",
                            Password = "2"
                        },
                        new
                        {
                            Id = 3,
                            Email = "thirdEmail@yandex.ru",
                            FirstName = "Иванов",
                            LastName = "Иван",
                            Login = "3",
                            Password = "3"
                        },
                        new
                        {
                            Id = 4,
                            Email = "fourthEmail@yandex.ru",
                            FirstName = "Петров",
                            LastName = "Пётр",
                            Login = "4",
                            Password = "4"
                        },
                        new
                        {
                            Id = 5,
                            Email = "fifthEmail@yandex.ru",
                            FirstName = "Баширов",
                            LastName = "Сидор",
                            Login = "5",
                            Password = "5"
                        });
                });

            modelBuilder.Entity("OtusTeaching.DB_Avito.Models.Сomment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int?>("PersonId")
                        .IsRequired()
                        .HasColumnType("integer");

                    b.Property<string>("Text")
                        .IsRequired()
                        .HasMaxLength(250)
                        .HasColumnType("character varying(250)");

                    b.HasKey("Id");

                    b.HasIndex("PersonId");

                    b.ToTable("Сomments");
                });

            modelBuilder.Entity("OtusTeaching.DB_Avito.Models.Advertisement", b =>
                {
                    b.HasOne("OtusTeaching.DB_Avito.Models.Person", "Person")
                        .WithMany("Advertisements")
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Person");
                });

            modelBuilder.Entity("OtusTeaching.DB_Avito.Models.Сomment", b =>
                {
                    b.HasOne("OtusTeaching.DB_Avito.Models.Person", null)
                        .WithMany("Сomments")
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("OtusTeaching.DB_Avito.Models.Person", b =>
                {
                    b.Navigation("Сomments");

                    b.Navigation("Advertisements");
                });
#pragma warning restore 612, 618
        }
    }
}
