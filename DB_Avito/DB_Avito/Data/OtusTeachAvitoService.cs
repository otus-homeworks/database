﻿using Microsoft.EntityFrameworkCore;
using OtusTeaching.DB_Avito.Models;
using OtusTeaching.DB_Avito.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OtusTeaching.DB_Avito.Data
{
    class OtusTeachAvitoService : IAvitoDataService
    {
        private readonly DBAvitoDataContext _DBcontext;
        public OtusTeachAvitoService(DBAvitoDataContext DataBaseContext)
        {
            _DBcontext = DataBaseContext;
        }
        public void ShowPersonsWithTheirAds()
        {
            Console.WriteLine("\nПользователи и их объявления");

            List<Person> persons = _DBcontext.Persons
                .Include(p => p.Advertisements)
                .ToList();

            foreach (Person prs in persons)
            {
                Console.WriteLine($"  Id:{prs.Id} {prs.FirstName} {prs.LastName}  ");

                if (prs.Advertisements.Count > 0)
                {
                    foreach (var ad in prs.Advertisements)
                        Console.WriteLine($"      Id:{ad.Id}  {ad.Text}  {ad.Cost} руб.");
                }
            }
        }

        public void ShowPersonsWithTheirComments()
        {
            Console.WriteLine("\nПользователи и отзывы к ним");

            List<Person> persons = _DBcontext.Persons
                .Include(p => p.Сomments)
                .ToList();

            foreach (Person prs in persons)
            {
                Console.WriteLine($"  Id:{prs.Id} {prs.FirstName} {prs.LastName}  ");

                if (prs.Сomments.Count > 0)
                {
                    foreach (var cm in prs.Сomments)
                        Console.WriteLine($"      Id:{cm.Id}  {cm.Text}");
                }
            }
        }

        public void AddPerson(Person person)
        {
            _DBcontext.Persons.Add(person);
            _DBcontext.SaveChanges();
        }

        public void AddAdvertisement(Person forPerson, Advertisement advertisement)
        {
            advertisement.Person = forPerson;
            advertisement.PersonId = forPerson.Id;
            _DBcontext.Advertisements.Add(advertisement);
            _DBcontext.SaveChanges();
            Console.WriteLine($"У пользователя {forPerson.Id} {forPerson.FirstName} {forPerson.LastName} добавлено объявление.");
        }

        public void AddComment(Person forPerson, Сomment comment)
        {
            comment.Person = forPerson;
            comment.PersonId = forPerson.Id;
            _DBcontext.Сomments.Add(comment);
            _DBcontext.SaveChanges();
            Console.WriteLine($"У пользователя {forPerson.Id} {forPerson.FirstName} {forPerson.LastName} добавлен комментарий.");
        }

        public Person GetRandomPerson()
        {
            Random rnd = new();
            return _DBcontext.Persons.ToArray()[rnd.Next(0, _DBcontext.Persons.Count())];
        }
    }
}
