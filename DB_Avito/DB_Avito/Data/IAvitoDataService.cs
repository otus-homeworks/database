﻿using OtusTeaching.DB_Avito.Models;

namespace OtusTeaching.DB_Avito.Data
{
    interface IAvitoDataService
    {
        public void ShowPersonsWithTheirAds();
        public void ShowPersonsWithTheirComments();
        public void AddPerson(Person person);
        public void AddAdvertisement(Person forPerson, Advertisement advertisement);
        public void AddComment(Person forPerson, Сomment comment);
        Person GetRandomPerson();
    }
}
