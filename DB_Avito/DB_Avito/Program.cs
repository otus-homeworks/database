﻿using OtusTeaching.DB_Avito.Data;
using OtusTeaching.DB_Avito.Models;
using OtusTeaching.DB_Avito.Seed;
using System;

namespace OtusTeaching.DB_Avito
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Имеющиеся в БД данные");

            using Repository.DBAvitoDataContext DataBaseContext = new();
            SetTrialDataToDB.Set(DataBaseContext);
            OtusTeachAvitoService AvitoService = new(DataBaseContext);

            AvitoService.ShowPersonsWithTheirAds();
            AvitoService.ShowPersonsWithTheirComments();

            AvitoService.AddPerson(new Person() { FirstName = "Добавленный", LastName = "Алекс", Email = "added@gmail.com", Login = "added", Password = "+1" });

            Person personWithAddedAdvertisement = AvitoService.GetRandomPerson();
            AvitoService.AddAdvertisement(personWithAddedAdvertisement, new Advertisement() { Category = AdCategory.Auto, Text = "Добавленное объявление", Cost = 50 });

            Person personWithAddedComment = AvitoService.GetRandomPerson();
            AvitoService.AddComment(personWithAddedComment, new Сomment() { Rating = RatingInComment.Excellent, Text = "Добавлена отличная отметка" });

            AvitoService.ShowPersonsWithTheirAds();
            AvitoService.ShowPersonsWithTheirComments();

            Console.WriteLine("Нажмите ввод для завершения");
            Console.ReadLine();
        }
    }
}
