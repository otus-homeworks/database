﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OtusTeaching.DB_Avito.Models
{
    public class Person
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(25)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(25)]
        public string LastName { get; set; }

        [MaxLength(25)]
        public string Email { get; set; }
        [MaxLength(12)]
        public string Phone { get; set; }

        [MaxLength(12)]
        public string Login { get; set; }

        [MaxLength(12)]
        public string Password { get; set; }

        public ICollection<Advertisement> Advertisements { get; set; }
        public ICollection<Сomment> Сomments { get; set; }
    }
}
