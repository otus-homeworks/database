﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtusTeaching.DB_Avito.Models
{
    public class Advertisement
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public AdCategory Category;

        [Required]
        [MaxLength(250)]
        public string Text { get; set; }

        [Required]
        public uint? Cost { get; set; }

        [Required]
        [ForeignKey("Person")]
        public int PersonId { get; set; }
        public Person Person { get; set; }
    }
}
