﻿namespace OtusTeaching.DB_Avito.Models
{
    public enum RatingInComment
    {
        None,
        Fail,
        Bad,
        Satisfactory,
        Good,
        Excellent
    }
}
