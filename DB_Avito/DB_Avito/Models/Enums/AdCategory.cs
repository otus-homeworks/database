﻿namespace OtusTeaching.DB_Avito.Models
{
    public enum AdCategory
    {
        Undef,
        Auto,
        Estate,
        Job,
        Services
    }
}
