﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OtusTeaching.DB_Avito.Models
{
    public class Сomment
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public Person Person;

        [Required]
        [ForeignKey("Person")]
        public int? PersonId { get; set; }

        [Required]
        public RatingInComment Rating;

        [Required]
        [MaxLength(250)]
        public string Text { get; set; }

    }
}
