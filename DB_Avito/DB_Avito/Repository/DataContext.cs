﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OtusTeaching.DB_Avito.Models;
using System;
using System.IO;

namespace OtusTeaching.DB_Avito.Repository
{
    public class DBAvitoDataContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Advertisement> Advertisements { get; set; }
        public DbSet<Сomment> Сomments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var configurationBuilder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            IConfigurationRoot configuration = configurationBuilder.Build();

            var connectionStr = configuration.GetConnectionString("Default");

            if (!string.IsNullOrEmpty(connectionStr))
                optionsBuilder.UseNpgsql(connectionStr);
            else
                throw new ApplicationException();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Person>().HasData(
                new Person[]
                    {
                        new Person() { Id = 1, FirstName = "Первый", LastName = "Василий", Email = "firstEmail@gmail.com", Login = "1", Password = "1" },
                        new Person() { Id = 2, FirstName = "Второй", LastName = "Антон", Email = "secondEmail@yandex.ru", Login = "2", Password = "2" },
                        new Person() { Id = 3, FirstName = "Иванов", LastName = "Иван", Email = "thirdEmail@yandex.ru", Login = "3", Password = "3" },
                        new Person() { Id = 4, FirstName = "Петров", LastName = "Пётр", Email = "fourthEmail@yandex.ru", Login = "4", Password = "4" },
                        new Person() { Id = 5, FirstName = "Баширов", LastName = "Сидор", Email = "fifthEmail@yandex.ru", Login = "5", Password = "5" }
                    });
        }
    }
}

